import terriblepack.ReallyBadClass;
import terriblepack.ReallyBadClassNumberTwo;

public class NewApp {
    public static void main(String[] args) {

        ReallyBadClass rbc = new ReallyBadClass();
        rbc.setI(null);

        if (1 == 2) {
            System.out.println("hello world");
            return;
        } else {
            System.out.println("hello new world");
        }

        ReallyBadClassNumberTwo rbcnt = new ReallyBadClassNumberTwo();
        try {
            if (rbcnt.getI().intValue() == -1) {
                System.out.println(rbcnt.toString());
            }
        } catch (Exception e) {

        }
    }
}